const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;

//routes

//create/add Product
router.post('/',verify,orderControllers.addOrder);

//get all orders
router.get('/',verify,verifyAdmin,orderControllers.getAllOrders);

//get user orders
router.get('/getUserOrders',verify,orderControllers.getUserOrders);


module.exports = router;